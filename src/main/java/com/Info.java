package com;

import java.text.DateFormatSymbols;
import java.text.NumberFormat;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.format.TextStyle;
import java.time.temporal.WeekFields;
import java.util.Arrays;
import java.util.Locale;

/**
 * This class displays information about a specific locale.
 */
public class Info {

    public static void showInfo(Locale locale) {
        if (locale == null)
            Info.localeInfo(Locale.getDefault());
        else
            Info.localeInfo(locale);
    }

    public static void localeInfo(Locale locale) {
        System.out.println("The received locale was: " + locale);
        System.out.println("Country: " + locale.getCountry());
        System.out.println("Language: " + locale.getLanguage());
        System.out.println("Currency: " + NumberFormat.getCurrencyInstance(locale).format(0));
        System.out.print("Weekdays: ");
        WeekFields wf = WeekFields.of(locale);
        DayOfWeek day = wf.getFirstDayOfWeek();
        for (int i = 0; i < DayOfWeek.values().length; i++) {
            System.out.print(day.getDisplayName(TextStyle.FULL, locale) + " ");
            day = day.plus(1);
        }
        System.out.println("");
        DateFormatSymbols date = new DateFormatSymbols(locale);
        String[] months = date.getMonths();
        System.out.print("Months: ");
        System.out.println(Arrays.toString(months));

        LocalDateTime today = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter
                .ofLocalizedDate(FormatStyle.FULL)
                .withLocale(locale);
        System.out.println("Today: " + today.format(formatter));
    }
}
