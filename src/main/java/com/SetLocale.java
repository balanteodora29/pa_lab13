package com;

import java.util.Locale;

/**
 * This class sets the application current locale.
 */
public class SetLocale {
    public static void set(Locale locale) {
        Locale.setDefault(locale);
    }
}
