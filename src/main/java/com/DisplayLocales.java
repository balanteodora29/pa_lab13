package com;

import java.util.Locale;

/**
 * This class displays all available locales.
 */
public class DisplayLocales {

    public static void execute(boolean console) {
        if (!console) {
            System.out.println("Default locale:");
            System.out.println(Locale.getDefault());
            System.out.println("Available locales:");
        }
        Locale available[] =
                Locale.getAvailableLocales();
        for (Locale locale : available) {
            System.out.println(locale.getDisplayCountry() + "\t" +
                    locale.getDisplayLanguage(locale));
        }
    }
}
