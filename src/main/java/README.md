# Laboratorul 13

[Link cerinta](https://profs.info.uaic.ro/~acf/java/labs/lab_13.html)

## Compulsory

* Create a package with the name res. Inside this package create the files: Messages.properties, Messages_ro.properties.
* Create the package com and implement the following classes describing commands:
    * DisplayLocales: to display all available locales
    * SetLocale: to set the application current locale.
    * Info: to display informations about the current or a specific locale.
    When the user sets a specific language tag, various information obtained using standard Java classes such as Currency or DateFormatSymbols should be displayed in a text area, as in the following example:
        * Country: Romania (România)
        * Language: Romanian (română)
        * Currency: RON (Romanian Leu)
        * Week Days: luni, marţi, miercuri, joi, vineri, sâmbătă, duminică
        * Months: ianuarie, februarie, martie, aprilie, mai, iunie, iulie, august, septembrie, octombrie, noiembrie, decembrie
        * Today: May 8, 2016 (8 mai 2016)
* Create the package app and the main class LocaleExplore. Inside this class, read commands from the keyboard and execute them. 
* All the locale-sensitive information should be translated in at least two languages (english-default and romanian), using the resource files.

## Rezolvare Compulsory

* Am creat pachetul res care contine fisierele Messages.properties si Messages_ro.properties.
* In pachetul com am creat clasele DisplayLocales, SetLocale si Info dupa cerinta.
* In pachetul app am creat clasa LocaleExplore care primeste comenzi si trimite raspunsul la consola.
  * list (shows all the available locales)
  * current (shows the current locale)  
  * set (change the current locale)
  * info (details about the current locale)  
Folosind set putem selecta limba/regiunea informatiilor (ro/en).