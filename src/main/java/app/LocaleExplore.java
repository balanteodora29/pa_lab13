package app;

import com.DisplayLocales;
import com.Info;
import com.SetLocale;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;

/**
 * This class allows you to read commands from the keyboard and execute them.
 */
public class LocaleExplore {

    /**
     * This method updates the locale and ResourceBundle based on user input.
     */
    public static ResourceBundle changeLocale(Locale locale) {
        String baseName = "res.Messages";
        ResourceBundle messages =
                ResourceBundle.getBundle(baseName, locale);
        SetLocale.set(locale);
        return messages;
    }

    public static ResourceBundle getLocaleMessages() {
        String baseName = "res.Messages";
        ResourceBundle messages =
                ResourceBundle.getBundle(baseName, Locale.getDefault());
        return messages;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        while (true) {
            ResourceBundle messages = LocaleExplore.getLocaleMessages();
            System.out.println(messages.getString("prompt"));
            String command = s.nextLine();
            LocaleExplore.execute(command);
        }
    }

    /**
     * This method executes the following commands given as parameter:
     * list (shows all the available locales)
     * current (shows the current locale)
     * set (change the current locale)
     * info (details about the current locale)
     */
    public static void execute(String command) {
        ResourceBundle messages = getLocaleMessages();
        String[] args = command.split(" ");
        switch (args[0]) {
            case ("list"):
                System.out.println(messages.getString("locales"));
                DisplayLocales.execute(true);
                break;
            case ("current"):
                String pattern = messages.getString("locale.set");
                Object[] arguments = {Locale.getDefault()};
                String welcome = new MessageFormat(pattern).format(arguments);
                System.out.println(welcome);
                break;
            case ("set"):
                String patternn = messages.getString("set");
                Object[] argumentss = {args[1]};
                String response = new MessageFormat(patternn).format(argumentss);
                if (args.length > 2)
                    changeLocale(new Locale(args[1], args[2]));
                else
                    changeLocale(new Locale(args[1]));
                System.out.println(response);
                break;
            case ("info"):
                String pat = messages.getString("info");
                Object[] argum = {Locale.getDefault()};
                String responsee = new MessageFormat(pat).format(argum);
                Info.showInfo(Locale.getDefault());
                break;
            default:
                System.out.println(messages.getString("invalid"));
        }
    }
}

